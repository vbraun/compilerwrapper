.. Compiler wrapper documentation master file, created by
   sphinx-quickstart on Mon Jan 10 15:15:48 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Compilerwrapper documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 2

   compilerwrapper

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

