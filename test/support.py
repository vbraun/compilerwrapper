"""
Support Library for Doctests
"""

import os, sys
from subprocess import Popen, PIPE, check_output


print check_output('./install_test_wrapper.sh')


class WrappedExecutable(object):
    
    def __init__(self, executable_file):
        self._file = executable_file
        self._executable = os.path.join('local', 'bin', executable_file)

    def __call__(self, *args):
        process = Popen([self._executable] + list(args), stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()
        if err:
            print "Error:"
            print err
            sys.exit(1)
        out = out.splitlines()
        assert out[0].endswith('test_executable.sh')
        print tuple([self._file] + out[1:])


cc =  WrappedExecutable('cc')
gcc = WrappedExecutable('gcc')
gplusplus = WrappedExecutable('g++')
c89 = WrappedExecutable('c89')
c99 = WrappedExecutable('c99')
ld =  WrappedExecutable('ld')
gfortran = WrappedExecutable('gfortran')
