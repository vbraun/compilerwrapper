#!/bin/sh

#set -x
set -e
if [ ! -f "install_test_wrapper.sh" ] ; then
    echo "You must run this script in the compilerwrapper test directory."
    exit 1
fi

TESTDIR=`pwd`
TOPDIR="$TESTDIR/.."

rm -rf "$TESTDIR/local"
mkdir -p "$TESTDIR/local/src"
cd "$TESTDIR/local/src"

for f in "$TOPDIR"/* ; do
    if [ ! -d "$f" ] ; then
	cp -p "$f" .
    fi
done
cp -rp "$TOPDIR"/src .
cp -rp "$TOPDIR"/m4 .

mkdir "$TESTDIR/local/src/test"
echo "all:" > "$TESTDIR/local/src/test/Makefile.in"
echo "install:" >> "$TESTDIR/local/src/test/Makefile.in"

mkdir "$TESTDIR/local/src/doc"
echo "all:" > "$TESTDIR/local/src/doc/Makefile.in"

./configure \
    --disable-doc \
    --prefix="$TESTDIR/local" \
    --with-ld="$TESTDIR/test_executable.sh" \
    --with-gfortran="$TESTDIR/test_executable.sh" \
    --with-ccpath="$TESTDIR" \
    --with-gcc-transform-name="s/.*/test_executable.sh/"
make
make install
