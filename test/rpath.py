#!/usr/bin/env python
"""
Test RPaths

EXAMPLES::

    >>> gcc('foo.c')
    ('gcc', 'foo.c', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
    >>> cc('foo.c')
    ('cc', 'foo.c', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
    >>> c89('foo.c')
    ('c89', 'foo.c', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
    >>> c99('foo.c')
    ('c99', 'foo.c', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
    >>> gplusplus('foo.cc')
    ('g++', 'foo.cc', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
    >>> ld('foo.o')
    ('ld', '--hash-style=both', 'foo.o', '-R', '$ORIGIN/../lib')
    >>> gfortran('foo.f')
    ('gfortran', 'foo.f', '-Wl,-R,$ORIGIN/../lib', '-Wl,--hash-style=both')
"""

from support import gcc, gplusplus, cc, c89, c99, ld, gfortran

if __name__ == "__main__":
    import doctest
    doctest.testmod()
