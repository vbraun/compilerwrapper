#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void libraryfunction(int *);

int main()
{
   int x;
   libraryfunction(&x);
   printf("main(): x=%d\n",x);

   printf("Enter a number: ");
   scanf("%d", &x);
   printf("You entered: %d\n", x);

   printf("malloc() in main.c\n");
   malloc(42);

   fflush(stdout);
   return 0;
}
      
