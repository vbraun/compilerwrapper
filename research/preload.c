#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>


static int (*real_open)(const char *, int, ...);
ssize_t (*real_write)(int fd, const void *buf, size_t count);
int (*real_printf)(const char *format, ...);
int (*real_scanf)(const char *format, ...);
void* (*real_malloc)(size_t size);


void __attribute__ ((constructor)) preload_initialize(void);
void __attribute__ ((destructor)) preload_finalize(void);

void preload_initialize(void)
{
  real_open = dlsym(RTLD_NEXT, "open");
  real_write = dlsym(RTLD_NEXT, "write");
  real_printf = dlsym(RTLD_NEXT, "printf");
  real_scanf = dlsym(RTLD_NEXT, "scanf");
  real_malloc = dlsym(RTLD_NEXT, "malloc");
}


void preload_finalize(void)
{
}





int printf(const char *format, ...)
{
  real_printf("[printf]  ");

  va_list ap;
  va_start(ap, format);
  int rc = vprintf(format, ap);
  va_end(ap);

  return rc;
}


void *malloc(size_t size)
{
  void* ptr = real_malloc(size);
  real_printf("[malloc] %x\n", ptr);
  return ptr;
}


int __isoc99_scanf(const char *format, ...)
{
  real_printf("[scanf]\n");

  va_list ap;
  va_start(ap, format);
  int rc = vscanf(format, ap);
  va_end(ap);

  return rc;
}


ssize_t write(int fd, const void *buf, size_t count)
{
  real_printf("[write]  ");

  ssize_t rc;
  rc = real_write(fd, buf, count);
  return rc;
}


int open(const char *pathname, int flags, ...)
{
  real_printf("[open]  ");

  int fd;
  fd = real_open(pathname, flags);
  return fd;
}

