#!/usr/bin/env python

import sys, os

if len(sys.argv) == 1:
    print 'Usage: ./audit_runpath.py <directory>\n'
    exit(0)

path = sys.argv[1]
dir = os.listdir(path)



def read_runpath(filename):
    fi, foe = os.popen4('readelf -d '+filename, 
                        mode='r', bufsize=-1)
    fi.close()
    result = foe.readlines()
    foe.close()
    if result[0].startswith('readelf: Error: Not an ELF file'):
        return 'Not an ELF file'
    for line in result:
        if '(RPATH)' in line:
            rpath = line[line.find('[') : line.find(']')+1]
            return rpath
    return None   # rpath not set



for dirent in dir:
    filename = os.path.join(path,dirent)
    if not os.path.isfile(filename):
        #print "Not a file: ", dirent
        continue
    if not os.access(filename, os.X_OK):
        #print "Not executable: ", dirent
        continue
    rpath = read_runpath(filename)
    print rpath, '\t', filename


