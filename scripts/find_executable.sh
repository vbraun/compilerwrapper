#!/usr/bin/env bash

usage()
{
    cat <<EOF 
Usage: find_executable.sh [--CC|--LD] [--exclude=<path>] [filename]

Traverses the \$PATH and finds the first executable named
"filename". You can slightly modify its behaviour with the options
described below. If no filename is specified eithe explictly or
implicitly by one of the options to the script, the help that you are
reading right now is displayed.

Options:

  --CC: Search for \$CC by default. Will try to detect compiler
        wrappers like ccache and find the underlying compiler
        installation.

  --LD: Search for \$LD by default.

  --exclude=<path>: Ignore any executables that can be found in the
         specified <path>.

Returns exit code 0 if "filename" was found and will print its full
qualified path name to stdout. Also returns 0 if this help was
displayed. Otherwise, exit code 1 is returned.
EOF
    exit 0
}


rightmost()
{
    # Return the rightmost command in a wrapper chain
    # For example: CC="ccache distcc /usr/local/bin/gcc-4"
    #              rightmost "$CC"
    # Returns:     /usr/local/bin/gcc-4
    OLD_IFS="$IFS"
    IFS=" "
    ARRAY=($1)
    IFS="$OLD_IFS"
    n=${#ARRAY[@]}
    if [ $n -gt 0 ]; then
	echo ${ARRAY[$n-1]}
    fi
}
# echo `rightmost "ccache gcc"`
# echo `rightmost "/bin/gcc"`
# echo `rightmost "distcc /usr/local/bin/gcc"`


if [ "$#" -eq 0 ]; then
    usage
fi


while [ $# -gt 0 ]; do
    if [ "$1" = "--CC" ];  then
	if [ "$mode_cc$mode_ld" != "" ]; then
	    usage
	fi
	mode_cc=true
	filename=`rightmost "$CC"`
	if [ "$filename" = "" ]; then
	    filename="gcc"
	fi
	shift
    else if [ "$1" = "--LD" ];  then
	if [ "$mode_cc$mode_ld" != "" ]; then
	    usage
	fi
	mode_ld=true
	filename=`rightmost "$LD"`
	if [ "$filename" = "" ]; then
	    filename="ld"
	fi
	shift
    else if [ "${1:0:9}" = "--exclude" ]; then
	exclude_dir="${1:10}"
	shift
    else
	filename=$1
	shift
    fi; fi; fi
done


if [ "$filename" = "" ]; then
    usage
fi


#echo "filename: $filename"
#echo "exclude directory: $exclude_dir"
#echo


# if the file name is a full qualified path to executable, return immediately
if [ -x "$filename" ]; then
    echo "$filename"
    exit 0
fi

# otherwise, search through $PATH
IFS=:
for p in $PATH ; do
    if [ "$p" = "$exclude_dir" ]; then
	#echo "skipping $p"
	continue
    fi
    fullname="$p/$filename"

    # skip over other compiler wrappers
    if [ -L "$fullname" ] && [ "$mode_cc" = "true" ]; then
	target=`ls -l $fullname | sed 's-.*/--'`
	if [ "$target" = "ccache" ] || [ "$target" = "distcc" ]; then
	    continue
	fi
    fi

    # return the full name
    if [ -x "$fullname" ]; then
	echo "$fullname"
	exit 0
    fi
done
