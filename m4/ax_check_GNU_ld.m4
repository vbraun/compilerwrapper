# SYNOPSIS
#
#   AX_CHECK_GNU_LD(LD_COMMAND, [ACTION-SUCCESS], [ACTION-FAILURE])
#
# DESCRIPTION
#
#   This macro tests whether $LD_COMMAND is GNU ld.
#   ACTION-SUCCESS/ACTION-FAILURE are shell commands to execute on
#   success/failure.
#
# LICENSE
#
#   Copyright (c) 2011 Volker Braun <vbraun.name@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or (at your
#   option) any later version.


dnl AX_CHECK_GNU_LD(LD_COMMAND, [ACTION-SUCCESS], [ACTION-FAILURE])

AC_DEFUN([AX_CHECK_GNU_LD],[
    AC_CACHE_CHECK(
        [whether $1 is GNU ld],
        ax_cv_prog_ld_is_gnu,
        [
            # dont use --version here, some GNU ld versions only accept -v.
            case `$1 -v 2>&1 </dev/null` in
            *GNU* | *'with BFD'*)
		ax_cv_prog_ld_is_gnu=yes
                ;;
            *)
		ax_cv_prog_ld_is_gnu=no
                ;;
            esac
        ]
    )
    if test "x$ax_cv_prog_ld_is_gnu" = xyes; then
	m4_default([$2], :)
    else
	m4_default([$3], :)
    fi
])dnl AX_CHECK_GNU_LD

