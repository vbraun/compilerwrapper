#!/bin/sh

set -e
if [ ! -f configure.ac ] ; then
    echo "You must run this script in the compilerwrapper root directory."
    exit 1
fi

TOPDIR=`pwd`
VERSION=`autoconf --trace='AC_INIT:$2'`

COMPILERWRAPPER_SPKG="$TOPDIR/spkg/compilerwrapper-$VERSION.spkg"
if [ ! -f $COMPILERWRAPPER_SPKG ] ; then
    echo "You must run make_spkg.sh first."
    exit 1
fi


cd "$TOPDIR/spkg/sage"
make distclean
cp "$COMPILERWRAPPER_SPKG" spkg/standard/

export SAGE_PARALLEL_SPKG_BUILD=yes
if [ -d /usr/lib64/atlas ] ; then
    export SAGE_ATLAS_LIB=/usr/lib64/atlas
fi

make build
