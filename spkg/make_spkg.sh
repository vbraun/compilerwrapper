#!/bin/sh

set -e
if [ ! -f configure.ac ] ; then
    echo "You must run this script in the compilerwrapper root directory."
    exit 1
fi

TOPDIR=`pwd`
VERSION=`autoconf --trace='AC_INIT:$2'`

TARBALL="$TOPDIR/compilerwrapper-$VERSION.tar.gz"
if [ ! -f $TARBALL ] ; then
    echo "You must make dist first."
    exit 1
fi

cd "$TOPDIR"
make dist

cd "$TOPDIR/spkg"
rm -rf "compilerwrapper-$VERSION"
cp -rap template "compilerwrapper-$VERSION"

cd "$TOPDIR/spkg/compilerwrapper-$VERSION"
tar xf "$TARBALL"
mv "compilerwrapper-$VERSION" src
echo 'src' > .hgignore
hg init
hg add .
hg commit -m "Updated to compilerwrapper-$VERSION"

cd "$TOPDIR/spkg"
tar cjf "compilerwrapper-$VERSION.spkg" "compilerwrapper-$VERSION"
