#include <iostream>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include "arguments.hh"
#include "path.hh"
#include "fixes.hh"
#include "install.hh"
#include "config.h"

using std::cout;
using std::endl;


void test()
{
  using std::string;
  assert( (path::file()/"test"/"1").str()  == string("test/1"));
  assert(  path::dir().str()               == string(""));
  assert( (path::dir()/"test"/"2").str()   == string("test/2/")); 
  assert( (path()/"test"/"3").str()        == string("test/3"));

  assert( (path::dir()  / "a" / "b" / "c").basename() == string("c"));
  assert( (path::file() / "a" / "b" / "c").basename() == string("c"));
  
  assert( path(".").is_relative() );
  assert( path("/usr").is_absolute() );
}



void usage()
{
  cout << "USAGE:" << endl;
  cout << "You probably don't want to run wrapper directly. " << endl
       << "The symlinks named 'gcc', 'cc', 'c99', 'c89', 'ld' (and similar" << endl
       << "if your compiler is not GCC) and pointing towards 'wrapper'" << endl
       << "will invoke the corresponding compiler/linker" << endl
       << "commands with some extra arguments." << endl
       << "Use 'wrapper <on|off>' to install/uninstall the symlinks" << endl
       << "appropriate for your compiler." << endl;
  
  cout << "WRAPPED_GCC" << " = " << path(CC_PATH_PREFIX)/WRAPPED_GCC_NAME << endl;
  cout << "WRAPPED_CC " << " = " << path(CC_PATH_PREFIX)/WRAPPED_GCC_NAME << endl;  
  cout << "WRAPPED_C99" << " = " << path(CC_PATH_PREFIX)/WRAPPED_C99_NAME << endl;
  cout << "WRAPPED_C89" << " = " << path(CC_PATH_PREFIX)/WRAPPED_C89_NAME << endl;
  cout << "WRAPPED_CPLUSPLUS" << " = " << path(CC_PATH_PREFIX)/WRAPPED_CPLUSPLUS_NAME << endl;
  cout << "WRAPPED_GPLUSPLUS" << " = " << path(CC_PATH_PREFIX)/WRAPPED_GPLUSPLUS_NAME << endl;
  cout << "WRAPPED_GFORTRAN " << " = " << path(GF_PATH_PREFIX)/WRAPPED_GFORTRAN_NAME << endl;
  cout << "WRAPPED_LD " << " = " << path(LD_PATH_PREFIX)/WRAPPED_LD_NAME << endl;

  test();
}


void modify_library_path()
{
  using std::string;
  char * new_path = NULL;

#ifdef __APPLE__
  const static string LD_LIBRARY_PATH = string("DYLD_LIBRARY_PATH");
#else
  const static string LD_LIBRARY_PATH = string("LD_LIBRARY_PATH");
#endif
  
  const string COMPILERWRAPPER_LD_LIBRARY_PATH = 
    string("COMPILERWRAPPER_") + LD_LIBRARY_PATH;
  if (new_path == NULL)
    new_path = getenv(COMPILERWRAPPER_LD_LIBRARY_PATH.c_str());

#ifdef WITH_SAGE
  const string SAGE_ORIG_LD_LIBRARY_PATH = 
    string("SAGE_ORIG_") + LD_LIBRARY_PATH;
  if (new_path == NULL)
    new_path = getenv(SAGE_ORIG_LD_LIBRARY_PATH.c_str());
#endif

  if (new_path == NULL)
    return;
  setenv(LD_LIBRARY_PATH.c_str(), new_path, true);

  if (getenv("COMPILERWRAPPER_DEBUG") != NULL) 
    std::cerr << "------- Changed library path --------------------" << endl
	      << LD_LIBRARY_PATH << " now is " << new_path << endl;
}


void modify_path()
{
  using std::string;
  char * new_path = NULL;

  const static string PATH = string("PATH");
  
  const string COMPILERWRAPPER_PATH = 
    string("COMPILERWRAPPER_") + PATH;
  if (new_path == NULL)
    new_path = getenv(COMPILERWRAPPER_PATH.c_str());

#ifdef WITH_SAGE
  const string SAGE_ORIG_PATH = 
    string("SAGE_ORIG_") + PATH;
  if (new_path == NULL)
      new_path = getenv(SAGE_ORIG_PATH.c_str());
#endif

  if (new_path == NULL)
    return;
  setenv(PATH.c_str(), new_path, true);

  if (getenv("COMPILERWRAPPER_DEBUG") != NULL) 
    std::cerr << "------- Changed path --------------------" << endl
	      << PATH << " now is " << new_path << endl;
}


int main(int argc, char *argv[])
{
  assert(argc > 0);
  Arguments args(argc, argv);
  const wrapped_program prog = identify_wrapped_program(argv[0]);
  modify_path();
  modify_library_path();

  //std::cerr << "------- executing --------------------" << endl
  //          << args;

  if (prog == WRAPPED_WRAPPER) {
    if (args.size() != 1) {
      usage();
      return 0;
    }
    std::string option = args.front();
    path wrapper = find_wrapper_executable(argv[0]);
    if (option == "on")  return create_wrapper_symlinks(wrapper);
    if (option == "off") return remove_wrapper_symlinks(wrapper);
    usage();
    return 1;
  }

  if (args.empty() or 
      args.is_single_option() or
      (prog == WRAPPED_GFORTRAN and 
       (args.contain("-v") or args.contain("-###")))) {
    // special case: don't add anything to empty argument list or
    // single "-option", probably version detection
    run_wrapped(prog, args);
    usage();
    return 0;
  }  

#ifdef __GNUC__
  if (prog == WRAPPED_LD) {
    ld_remove_preload(args);
    ld_remove_runpaths(args);
    ld_add_runpath(args);
    ld_add_relocation(args);
    ld_add_preload(args);
  }
#endif // __GNUC__

#ifdef __GNUC__
  if (prog ==  WRAPPED_GCC ||
      prog ==  WRAPPED_CC ||
      prog ==  WRAPPED_C99 ||
      prog ==  WRAPPED_C89 ||
      prog ==  WRAPPED_CPLUSPLUS ||
      prog ==  WRAPPED_GPLUSPLUS ||
      prog ==  WRAPPED_GFORTRAN) {
    // Note: gcc can be configured to call ld in some non-standard
    // path. So we cannot rely on our ld wrapper to be called by gcc,
    // and we must to modify the -Wl,<ld-options>.
    gcc_remove_preload(args);
    gcc_remove_dangerous_optimizations(args);
    gcc_remove_runpaths(args);
    gcc_add_runpath(args);
    gcc_add_relocation(args);
    gcc_add_preload(args);
  }
#endif // __GNUC__
  
  run_wrapped(prog, args); // only returns if prog == WRAPPED_UNKNOWN
  usage();
  return 0;
}
