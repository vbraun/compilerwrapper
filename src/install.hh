#ifndef INSTALL__HH
#define INSTALL__HH

#include "path.hh"

path find_wrapper_executable(std::string argv_zero);

int create_wrapper_symlinks(const path& wrapper_filename, bool verbose=true);
int remove_wrapper_symlinks(const path& wrapper_filename, bool verbose=true);

#endif
