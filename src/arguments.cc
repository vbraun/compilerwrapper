#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <sstream>
#include <iterator>
#include <algorithm>
#include "arguments.hh"
#include "path.hh"
#include "config.h"

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::stringstream;

wrapped_program identify_wrapped_program(const char* filename_c)
{
  string cmd = path(filename_c).basename();

  if (cmd == string(WRAPPER_NAME)) return WRAPPED_WRAPPER;

#ifdef __GNUC__
  if (cmd == string(WRAPPER_GCC_NAME))       return WRAPPED_GCC;
  if (cmd == string(WRAPPER_CC_NAME))        return WRAPPED_CC;
  if (cmd == string(WRAPPER_C99_NAME))       return WRAPPED_C99;
  if (cmd == string(WRAPPER_C89_NAME))       return WRAPPED_C89;
  if (cmd == string(WRAPPER_CPLUSPLUS_NAME)) return WRAPPED_CPLUSPLUS;
  if (cmd == string(WRAPPER_GPLUSPLUS_NAME)) return WRAPPED_GPLUSPLUS;
  if (cmd == string(WRAPPER_GFORTRAN_NAME))  return WRAPPED_GFORTRAN;
  if (cmd == string(WRAPPER_LD_NAME))        return WRAPPED_LD;
  if (cmd == string(WRAPPER_BUILD_ALIAS "-" WRAPPER_GCC_NAME)) return WRAPPED_GCC;
  if (cmd == string(WRAPPER_BUILD_ALIAS "-" WRAPPER_CPLUSPLUS_NAME)) return WRAPPED_CPLUSPLUS;
  if (cmd == string(WRAPPER_BUILD_ALIAS "-" WRAPPER_GPLUSPLUS_NAME)) return WRAPPED_GPLUSPLUS;
  if (cmd == string(WRAPPER_BUILD_ALIAS "-" WRAPPER_GFORTRAN_NAME)) return WRAPPED_GFORTRAN;
#endif // __GNUC__

  return WRAPPED_UNKNOWN;
}


void run_wrapped(wrapped_program prog, const Arguments& arg)
{
  path cmd;
  switch (prog) {

#ifdef __GNUC__
  case WRAPPED_GCC:       cmd = path(CC_PATH_PREFIX)/WRAPPED_GCC_NAME;  break;
  case WRAPPED_CC:        cmd = path(CC_PATH_PREFIX)/WRAPPED_GCC_NAME;  break;
  case WRAPPED_C99:       cmd = path(CC_PATH_PREFIX)/WRAPPED_C99_NAME;  break;
  case WRAPPED_C89:       cmd = path(CC_PATH_PREFIX)/WRAPPED_C89_NAME;  break;
  case WRAPPED_CPLUSPLUS: cmd = path(CC_PATH_PREFIX)/WRAPPED_CPLUSPLUS_NAME;  break;
  case WRAPPED_GPLUSPLUS: cmd = path(CC_PATH_PREFIX)/WRAPPED_GPLUSPLUS_NAME;  break;
  case WRAPPED_GFORTRAN:  cmd = path(GF_PATH_PREFIX)/WRAPPED_GFORTRAN_NAME;  break;
  case WRAPPED_LD:        cmd = path(LD_PATH_PREFIX)/WRAPPED_LD_NAME;    break;
#endif // __GNUC__

  case WRAPPED_WRAPPER:
  default: 
    assert(false); return;
  }
  Arguments arg_cpy = arg;
  arg_cpy.insert(arg_cpy.begin(), cmd.str());

  char* const* argv= arg_cpy.argv();
  execv(argv[0], argv);

  // if execv returns, there must have been an error
  cerr << "Unknown command " << cmd << endl;
  exit(1);
}


/////////////////////////////////////////////////

Arguments::Arguments(int argc_, char** argv_)
  : tmp_argv(NULL) 
{
  assert(argc_ >= 1);
  for (int i=1; i<argc_; i++) {
    string arg(argv_[i]);
    push_back(arg);
  }
}


void Arguments::construct_argv() const
{
  typedef char* char_ptr;
  tmp_argv = new char_ptr[size()+1];

  const_iterator argi = begin();
  for (size_t i=0; i<size(); i++)
    tmp_argv[i] = const_cast<char*>( (*argi++).c_str() );
  assert(argi==end());
  tmp_argv[size()] = NULL;


  if (getenv("COMPILERWRAPPER_DEBUG") != NULL) 
    cerr << "------- rewritten arguments --------------------" << endl
	 << (*this) << endl;
}


void Arguments::destroy_argv() const
{
  if (tmp_argv==NULL) return;
  delete [] tmp_argv;
  tmp_argv = NULL;
}


char ** Arguments::argv() const
{
  destroy_argv();
  construct_argv();
  return tmp_argv;
}


std::ostream& operator << (std::ostream& out, const Arguments& arg)
{
  size_t pos = 0;
  for (Arguments::const_iterator argi = arg.begin(); argi != arg.end(); argi++)
    out << "Argument " << pos++ << ": " << *argi << endl;
  return out;
}


void Arguments::try_remove_arg(const string& arg, int following)
{
  for (iterator ai = begin(); ai != end(); ) {
    if (*ai==arg) {
      ai = erase(ai);        // erase arg
      for (int i=0; i<following; i++) {
	assert(ai != end()); 
	ai = erase(ai);      // erase i-th following arg
      }
    } else
      ai++;                  // advance to next arg
  }
}







namespace {
  string extract_quoted_substr(string& remaining) {
    // TODO: allow for escapes inside quotes: "text\"text"
    const char delimiter = remaining[0];
    size_t end_quote = remaining.find_first_of(delimiter, 1);
    if (end_quote==string::npos)
      cerr << "gcc wrapper: unbalanced quote" << endl;
    const string token = remaining.substr(1,end_quote-1);
    remaining.erase(0, end_quote);
    return token;
  }
}


Arguments::Arguments(const string& input, const string& delimiter)
  : tmp_argv(NULL) 
{
  if (input.empty()) return;

  static const char SINGLE_QUOTE = '\'';
  static const char DOUBLE_QUOTE = '"';

  size_t n_delimiter = delimiter.size();
  string remaining = input;
  string accumulator;

  while (remaining.size() > 0) {

    if (remaining.substr(0,n_delimiter) == delimiter) {
      remaining.erase(0, n_delimiter);
      push_back(accumulator);  
      accumulator.clear();
      continue;
    }

    // TODO: allow for escapes inside quotes: "text\"text"
    if (remaining[0] == SINGLE_QUOTE || remaining[0] == DOUBLE_QUOTE) {
      const string quoted_token = extract_quoted_substr(remaining);
      push_back(quoted_token);
    }

    accumulator += remaining.substr(0, 1);
    remaining.erase(0, 1);
  }
  push_back(accumulator);  
}


string Arguments::join(const string& delimiter) const
{
  stringstream ss;
  const_iterator ai = begin();
  if (ai == end()) 
    return string();
  while (true) {
    ss << *ai++;
    if (ai == end()) 
      return ss.str();
    ss << delimiter;
  }
}


bool Arguments::is_single_option() const
{
  return size()==1 and front()[0]=='-';
}

bool Arguments::contain(const std::string& arg) const
{
  return std::find(begin(), end(), arg) != end();
}
