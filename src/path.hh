#ifndef PATH__HH
#define PATH__HH

#include <string>
#include <vector>

class path
{
protected:
  std::vector<std::string> data;
  void parse(const std::string& name);
  typedef std::vector<std::string>::iterator iterator;
  typedef std::vector<std::string>::const_iterator const_iterator;
public:
  static std::string separator();
  static std::string current_dir();
  static path file();
  static path dir();

  path() : data() {};
  path(const char* name) : data() { parse(name); };
  path(const std::string& name) : data() { parse(name); };
  
  path operator / (const path& other) const;

  std::string str() const;
  void canonicalize();
  
  friend std::ostream& operator << (std::ostream& out, const path& p);
  bool is_dir() const;
  bool is_file() const;
  bool is_relative() const;
  bool is_absolute() const { return not is_relative(); }
  
  bool file_exists() const;

  std::string basename() const;
  std::string dirname() const;
  path directory() const;
  path full_qualified() const;
};



#endif

