#include <sstream>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <unistd.h>
#include "path.hh"

using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::vector;


string path::separator()
{
  return string("/");   // all real operating systems
}


string path::current_dir()
{
  return string(".");   // all real operating systems
}


path path::file()
{
  return path();
}


path path::dir()
{
  path newpath;
  newpath.data.push_back(string());
  return newpath;
}


void path::parse(const std::string& name)
{
  stringstream ss(name);
  string item;
  const char sep = path::separator()[0];  // only real operating systems
  while(std::getline(ss, item, sep))
    data.push_back(item);
}


path path::operator / (const path& other) const
{
  path newpath;
  newpath.data.insert(newpath.data.end(), data.begin(), data.end());
  if (is_dir() && data.size()>0) newpath.data.pop_back();
  newpath.data.insert(newpath.data.end(), other.data.begin(), other.data.end());
  if (is_dir() || other.is_dir()) newpath.data.push_back(string());
  return newpath;
}


string path::str() const
{
  stringstream ss;
  for (vector<string>::const_iterator si = data.begin();
       si != data.end(); si++) {
    if (si != data.begin())
      ss << separator();
    ss << (*si);
  }
  return ss.str();
}



bool path::is_dir() const
{
  size_t n = data.size();
  if (n == 0) return false;
  return data[n-1].empty();
}


bool path::is_file() const
{
  return !is_dir();
}


bool path::file_exists() const
{
  return access(str().c_str(), F_OK) == 0;
}


bool path::is_relative() const
{
  if (data.empty())
    return true;
  return not data[0].empty();
}



string path::basename() const
{
  if (is_file())
    if (data.size() == 0)
      return string();
    else
      return data.back();
  if (is_dir()) {
    assert(data.size() > 0);
    if (data.size() == 1)
      return string();
    else
      return data[data.size()-2];
  }
}


string path::dirname() const
{
  return directory().str();
}


path path::directory() const
{
  path p = (*this);
  if (p.is_file())
    if (p.data.size() > 0) {
      p.data.erase(p.data.end()-1);
      return p;
    } else
      return path(".");
  return p;
}




path path::full_qualified() const
{
  using std::vector;
  if (is_absolute()) 
    return (*this);
  path fqn;
  bool successfull_getcwd = false;
  for (int n=128; n<4096; n+=128) {
    vector<char> cwd(n);
    char *rc = getcwd(&cwd.front(), cwd.size());
    if (rc!=NULL) {
      fqn = path(&cwd.front());
      successfull_getcwd = true;
      break;
    }
    cout << "fqn getcwd() failure() " << n << endl;
  }
  if (not successfull_getcwd) {
    cout << "Could not get current working directory. Quitting." << endl;
    exit(3);
  }
  fqn.data.insert(fqn.data.end(), data.begin(), data.end());
  fqn.canonicalize();
  return fqn;
}


void path::canonicalize()
{
  for (iterator pi = data.begin(); pi != data.end(); ) {
    if ((*pi) == ".")
      pi = data.erase(pi);
    else
      pi++;
  }
}



std::ostream& operator << (std::ostream& out, const path& p)
{
  out << p.str();
  return out;
}
