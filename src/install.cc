#include <iostream>
#include <unistd.h>
#include <string>
#include <list>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "install.hh"
#include "config.h"


using std::cout;
using std::endl;
using std::string;
using std::list;

namespace {

  class symlink_pair
  {
  public:
    symlink_pair(const path& src, const path& dst) 
      : source(src.str()), target(dst.str()) {};
    string source;
    string target;
  };

  list<path> symlink_list(const path& wrapper)
  {
    list<path> result;
    path directory = wrapper.directory();

    std::back_insert_iterator< list<path> > 
      bii = back_inserter(result);

#ifdef __GNUC__
    *bii = directory/WRAPPER_GCC_NAME;
    *bii = directory/WRAPPER_CC_NAME;
    *bii = directory/WRAPPER_C89_NAME;
    *bii = directory/WRAPPER_C99_NAME;
    *bii = directory/WRAPPER_CPLUSPLUS_NAME;
    *bii = directory/WRAPPER_GPLUSPLUS_NAME;
    *bii = directory/WRAPPER_GFORTRAN_NAME;
    *bii = directory/WRAPPER_LD_NAME;
    if (!string(WRAPPER_BUILD_ALIAS).empty()) {
      *bii = directory/WRAPPER_BUILD_ALIAS "-" WRAPPER_GCC_NAME;
      *bii = directory/WRAPPER_BUILD_ALIAS "-" WRAPPER_CPLUSPLUS_NAME;
      *bii = directory/WRAPPER_BUILD_ALIAS "-" WRAPPER_GPLUSPLUS_NAME;
      *bii = directory/WRAPPER_BUILD_ALIAS "-" WRAPPER_GFORTRAN_NAME;
    }
#endif // __GNUC__


    return result;
  }

} // anonymous namespace




int create_wrapper_symlinks(const path& wrapper_filename, bool verbose)
{
  remove_wrapper_symlinks(wrapper_filename, false);
  if (verbose) cout << "Creating symlinks..." << endl;

  list<path> symlinks = symlink_list(wrapper_filename);
  path wrapper = wrapper_filename.full_qualified();

  int rc = 0;
  int errsv;
  for (list<path>::const_iterator li = symlinks.begin(); li != symlinks.end(); li++) {
    if (verbose) cout << *li << "  ->  " << wrapper << endl;
    rc = symlink(wrapper.str().c_str(), li->str().c_str());
    if (rc != 0) { 
      errsv = errno;
      break;
    }
  }
  if (rc!=0) cout << "Error: " << strerror(errsv) << endl;
  return rc;
}


int remove_wrapper_symlinks(const path& wrapper_filename, bool verbose)
{
  if (verbose) cout << "Removing symlinks..." << endl;
  path dir = wrapper_filename.directory();

  list<path> symlinks = symlink_list(wrapper_filename);
  int rc = 0;
  int errsv;
  for (list<path>::const_iterator li = symlinks.begin(); li != symlinks.end(); li++) {
    if (verbose) cout << "removing " << *li << endl;
    rc = unlink(li->str().c_str());
    if (rc!=0)
      if (errno==ENOENT) 
	rc = 0;  // file not found, so what
      else {
	errsv = errno;
	break;
      } 
  }
  if (rc!=0) cout << "Error: " << strerror(errsv) << endl;
  return rc;
}


path find_wrapper_executable(string argv_zero)
{
  path wrapper(argv_zero);
  if (not wrapper.is_absolute())
    wrapper = wrapper.full_qualified();
  if (wrapper.file_exists())
    return wrapper;

  const char *SAGE_LOCAL =  getenv("SAGE_LOCAL");
  if (SAGE_LOCAL != NULL) {
    wrapper = path(SAGE_LOCAL) / "bin" / "wrapper";
    wrapper = wrapper.full_qualified();
    if (wrapper.file_exists())
      return wrapper;
  }

  cout << "Error: cannot find " << argv_zero << ". "
       << "Maybe set SAGE_LOCAL?" << endl;
  exit(3);
}



