#ifndef ARGUMENT__HH
#define ARGUMENT__HH

#include <string>
#include <list>
#include <iostream>

class Arguments;

enum wrapped_program {
  WRAPPED_WRAPPER,
  WRAPPED_GCC, 
  WRAPPED_CC, 
  WRAPPED_C99, 
  WRAPPED_C89, 
  WRAPPED_LD,
  WRAPPED_CPLUSPLUS, 
  WRAPPED_GPLUSPLUS, 
  WRAPPED_GFORTRAN,
  WRAPPED_UNKNOWN
}; 

wrapped_program identify_wrapped_program(const char*);
void run_wrapped(wrapped_program prog, const Arguments& arg);


class Arguments: public std::list<std::string>
{
private:
  mutable char** tmp_argv;
  void construct_argv() const;
  void destroy_argv() const;
public:
  Arguments(): tmp_argv(NULL) {};
  Arguments(const std::string& input, const std::string& delimiter = std::string(" "));
  Arguments(int argc_, char ** argv_);

  // remove all occurences of arg + following args
  void try_remove_arg(const std::string& arg, int following=0);

  // Return an argument list suitable for execv
  char ** argv() const;
  
  // Return whether the arguments consist of a single "-option"
  bool is_single_option() const;

  // Return whether the arguments contains the given one
  bool contain(const std::string& arg) const;
  
  friend std::ostream& operator << (std::ostream& out, const Arguments& arg);
  std::string join(const std::string& delimiter = std::string(" ")) const;
};



#endif
