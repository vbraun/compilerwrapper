#include <cassert>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include "fixes.hh"
#include "path.hh"
#include "config.h"


namespace {
  using std::stringstream;
  using std::string;
  using std::cout;
  using std::endl;

  // Return whether gcc is about to link.
  /* That is, produce an executable or shared library. Unfortunately
   * there is no single switch to look for. Instead, we have to check
   * for switches that *do not* perform the linking step.
   */
  bool gcc_does_not_link(const Arguments& args)
  {
    // configure tries some illegal args to test for version
    if (args.is_single_option()) return true;

    Arguments nolink;
    nolink.push_back("-c");
    nolink.push_back("-S");
    nolink.push_back("-E");
    for (Arguments::const_iterator ai = args.begin(); ai != args.end(); ai++)
      for (Arguments::const_iterator noli = nolink.begin(); noli != nolink.end(); noli++)
	if ((*ai) == (*noli))
	  return true;
    return false;
  }

  bool starts_with(const string& whole_string, const string& s)
  {
    size_t n = s.size();
    if (whole_string.size() < n)
      return false;
    return whole_string.substr(0,n) == s;
  }

  bool ends_with(const string& whole_string, const string& s)
  {
    size_t n = s.size();
    if (whole_string.size() >= s.size())
      return (0 == whole_string.compare(whole_string.size()-n, n, s));
    return false;
  }

  // add -Wl,<ld_add_args()> right before the specified position arg_iter
  void gcc_passthrough_ld_add(argument_add_func ld_add_args, 
			      Arguments& args, Arguments::iterator arg_iter)
  {
    if (gcc_does_not_link(args)) return;
    Arguments ld_args;
    ld_add_args(ld_args);
    if (ld_args.empty()) return;      
    ld_args.insert(ld_args.begin(), "-Wl");
    args.insert(args.end(), ld_args.join(","));
  }

  // add -Wl,<ld_add_args()> at the end of the gcc command line
  void gcc_passthrough_ld_add(argument_add_func ld_add_args, Arguments& args)
  {
    gcc_passthrough_ld_add(ld_add_args, args, args.end());
  }

  // parse -Wl,<ld_args1> -Wl,<ld_args2> and apply ld_modify_args(ld_args1+ld_args2)
  void gcc_passthrough_ld_modify(argument_modifier_func ld_modify_args, Arguments& args)
  {
    if (gcc_does_not_link(args)) return;
    for (Arguments::iterator ai = args.begin(); ai != args.end(); ) {

      // collect -Wl,... -Wl,... arguments
      Arguments collected_ld_args;
      Arguments::iterator collected_args_start = ai;
      while (ai != args.end()) {
	if (not starts_with(*ai,"-Wl")) break;
	Arguments ld_args(*ai++, ",");
	ld_args.erase(ld_args.begin());  // throw away -Wl
	collected_ld_args.insert(collected_ld_args.end(), 
				 ld_args.begin(), ld_args.end());
      }

      if (collected_ld_args.empty()) {  
	// collected zero or more times -Wl without ","
	args.erase(collected_args_start, ai);
	ai++;
	continue;
      }
	
      // modify the linker args
      ld_modify_args(collected_ld_args);

      // Re-escape as on -Wl,... option
      args.erase(collected_args_start, ai);
      if (not collected_ld_args.empty()) {
	collected_ld_args.insert(collected_ld_args.begin(), "-Wl");
	args.insert(ai, collected_ld_args.join(","));
      }
    }
    // cout << "modified args: " << endl << args << endl;
  }

} // anonymous namespace




/*******************************************************
 *  
 *   Options for ld
 *
 *******************************************************/

void ld_remove_runpaths(Arguments &args)
{
  for (Arguments::iterator ai = args.begin(); ai != args.end(); ) {
#ifdef LINKER_IS_APPLE_OSX_LD
    if (*ai=="-install_name") {
#else
    if (*ai=="-R" || *ai=="-rpath" || *ai=="--rpath") {
#endif
      ai = args.erase(ai);        // erase "-R"
      assert(ai != args.end()); 
      ai = args.erase(ai);        // erase <path>
    } else
      ai++;                       // advance to next arg
  }
}


void ld_add_runpath(Arguments &args)
{
  // add a relative rpath
#ifdef LINKER_IS_APPLE_OSX_LD
  args.insert(args.end(), "-install_name");
  args.insert(args.end(), "@executable_path/../lib");
#else
  // Solaris only understands -R and not -rpath/--rpath
  // GNU is fine with either option
  args.insert(args.end(), "-R");
  args.insert(args.end(), "$ORIGIN/../lib");
#endif

  // Also add a static (full qualified) path. During build, configure
  // scripts will build and run files outside of $SAGE_LOCAL/bin.
  const char *SAGE_LOCAL =  getenv("SAGE_LOCAL");
  if (SAGE_LOCAL != NULL) {
    const path library_path = path(SAGE_LOCAL) / "lib";
#ifdef LINKER_IS_APPLE_OSX_LD
    args.insert(args.end(), "-install_name");
#else
    args.insert(args.end(), "-R");
#endif
    args.insert(args.end(), library_path.str());
  }
}


void ld_add_relocation(Arguments &args)
{
#ifdef LD_HAS_HASH_STYLE
  // Some older binutils get confused with sysv vs. gnu-style ELF hash
  // sections. See http://trac.sagemath.org/sage_trac/ticket/9379
  args.insert(args.begin(), "--hash-style=both");
#endif
}


void ld_remove_preload(Arguments &args)
{
  const char* COMPILERWRAPPER_LDFLAGS = getenv("COMPILERWRAPPER_LDFLAGS");
  if (COMPILERWRAPPER_LDFLAGS == NULL) return;
  Arguments preload(COMPILERWRAPPER_LDFLAGS, ",");
  
  for (Arguments::iterator 
	 preload_i = preload.begin(); preload_i != preload.end(); preload_i++) {
    string library_arg = string("-l") + (*preload_i);
    for (Arguments::iterator ai = args.begin(); ai != args.end(); )
      if (*ai == library_arg)
	ai = args.erase(ai);
      else
	ai++;
  }
}


void ld_add_preload(Arguments &args)
{
  const char* COMPILERWRAPPER_LDFLAGS = getenv("COMPILERWRAPPER_LDFLAGS");
  if (COMPILERWRAPPER_LDFLAGS == NULL) return;
  Arguments preload(COMPILERWRAPPER_LDFLAGS, ",");
  
  Arguments::iterator args_begin = args.begin();
  for (Arguments::iterator 
	 preload_i = preload.begin(); preload_i != preload.end(); preload_i++) {
    string library_arg = (*preload_i);
    args.insert(args_begin, library_arg);
  }
}



/*******************************************************
 *  
 *   Options for gcc
 *
 *******************************************************/

void gcc_add_relocation(Arguments &args)
{
  gcc_passthrough_ld_add(ld_add_relocation, args);
}


void gcc_remove_runpaths(Arguments &args)
{
  gcc_passthrough_ld_modify(ld_remove_runpaths, args);
  // R-project manages to get the linker flags from gfortran and then
  // hands them to gcc (wat! totally illegal). Workaround: strip the
  // ld runpaths.
  ld_remove_runpaths(args);
}


void gcc_add_install_name(Arguments &args)
{
#ifndef LINKER_IS_APPLE_OSX_LD
  return;
#endif
  string dylib_name;
  for (Arguments::const_iterator ai = args.begin(); ai != args.end();  ) 
    if (*ai++ == "-o") {
      assert(ai!=args.end()); // there must be a value following -o
      path output = *ai++;
      dylib_name = output.basename();
    }
  if (not ends_with(dylib_name, ".dylib"))
    return;
  args.insert(args.end(), "-Wl,-install_name,@executable_path/../lib/"+dylib_name);
}


void gcc_add_runpath(Arguments &args)
{
#ifdef LINKER_IS_APPLE_OSX_LD
  gcc_add_install_name(args);
#else
  gcc_passthrough_ld_add(ld_add_runpath, args);
#endif
}



void gcc_limit_optimization(Arguments &args, int O_max)
{
  using std::string;
  using std::istringstream;
  using std::ostringstream;

  for (Arguments::iterator ai = args.begin(); ai != args.end(); ai++) {
    string &arg = *ai;
    if (arg.substr(0,2) != "-O")
      continue;

    // we must make sure to not modify -OPT:Olimit=0 (not a gcc
    // option, some scripts check that gcc errors out here)

    istringstream iss(arg.substr(2));
    int O_curr = 1;   // gcc default: -O is equivalent to -O1
    iss >> O_curr;
    if (iss.eof()) {  // all of iss got converted into integer O_curr
      ostringstream oss;
      oss << "-O" << std::min(O_curr, O_max);
      arg = oss.str();
    } 
  }
}


void gcc_remove_dangerous_optimizations(Arguments &args)
{

#if defined(__ia64__)
  // On itanium, -O3 and -funroll-loops are broken
  // and will probably never get fixed (dying arch)
  // http://gcc.gnu.org/bugzilla/show_bug.cgi?id=46044
  gcc_limit_optimization(args, 2);
  args.try_remove_arg("-funroll-loops");
#endif


#if (__GNUC__ == 4 && __GNUC_MINOR__ == 4 &&  __GNUC_PATCHLEVEL__ == 1)
  // gcc 4.4.1 with -O2 and higher sometimes hangs and eats all memory
  gcc_limit_optimization(args, 1);
#endif
}



void gcc_remove_preload(Arguments &args)
{
  if (gcc_does_not_link(args)) return;
  // You could specify gcc -lxxx or gcc -Wl,-lxxx to link with libxxx.so
  gcc_passthrough_ld_modify(ld_remove_preload, args);
  ld_remove_preload(args);
}


void gcc_add_preload(Arguments &args)
{
  if (gcc_does_not_link(args)) return;
  ld_add_preload(args);
}

