#ifndef FIXES__HH
#define FIXES__HH

#include "arguments.hh"


// A function that *only* adds arguments
typedef void (*argument_add_func)(Arguments&);

// a function that *only* modifies (overwrite and delete but not add) arguments
typedef void (*argument_modifier_func)(Arguments&);


/*******************************************************
 *   Options for ld
 *******************************************************/

// remove all runpaths (libtool likes to set these)
void ld_remove_runpaths(Arguments &args);

// add $ORIGIN/../lib rpath
void ld_add_runpath(Arguments &args);

// add options that improve the chances of relocating the binaries
void ld_add_relocation(Arguments &args);

// remove libraries from the GCCWRAP_PRELOAD environment variable
void ld_remove_preload(Arguments &args);

// add libraries from the GCCWRAP_PRELOAD environment variable
void ld_add_preload(Arguments &args);


/*******************************************************
 *   Options for gcc
 *******************************************************/

// Limit -Ox to x<=O_max
void gcc_limit_optimization(Arguments &args, int O_max);

// fixes for specific archs and gcc versions
void gcc_remove_dangerous_optimizations(Arguments &args);

// remove all runpaths passed to the gcc command line with -Wl
void gcc_remove_runpaths(Arguments &args);

// add $ORIGIN/../lib rpath to the gcc command line with -Wl
void gcc_add_runpath(Arguments &args);

// Add -Wl,-install_path,@executable_path/../lib/library.dylib (OSX only)
void gcc_add_install_name(Arguments &args);

// add options that improve the chances of relocating the binaries
void gcc_add_relocation(Arguments &args);

// remove libraries from the GCCWRAP_PRELOAD environment variable
void gcc_remove_preload(Arguments &args);

// add libraries from the GCCWRAP_PRELOAD environment variable
void gcc_add_preload(Arguments &args);


#endif 
